'use strict';

import gulp from "gulp";
import del from "del";
import runSequence from "run-sequence";
import browserSync from "browser-sync";
import gulpLoadPlugins from "gulp-load-plugins";
import babelify from "babelify";
import browserify from "browserify";
import source from "vinyl-source-stream";
import gutil from "gulp-util";
import pug from "gulp-pug";
import jsoncombine from "gulp-jsoncombine";

const $ = gulpLoadPlugins();
const reload = browserSync.reload;

const browserifyScript = (script) => {
    return browserify({
        entries: `./src/js/${script}`
    })
        .transform(babelify.configure({
            presets: [`es2015`]
        }))
        .bundle()
        .on('error', function (err) {
            gutil.log(gutil.colors.red.bold(`[browserify error]`));
            gutil.log(err.message);
            gutil.log(err);
            this.emit(`end`);
        })
        .pipe(source(script))
        .pipe(gulp.dest('./public/assets/js'));
};

gulp.task('styles', () => {
    const AUTOPREFIXER_BROWSERS = [
        'ie >= 10',
        'ie_mob >= 10',
        'ff >= 30',
        'chrome >= 34',
        'safari >= 7',
        'opera >= 23',
        'ios >= 7',
        'android >= 4.4',
        'bb >= 10'
    ];

    // For best performance, don't add Sass partials to `gulp.src`
    return gulp.src([
        './src/scss/**/*.scss'
    ])
        .pipe($.newer('.tmp/styles'))
        .pipe($.sourcemaps.init())
        .pipe($.sass({
            precision: 10,
            includePaths: [
                'node_modules/normalize-scss/sass',
                'node_modules/font-awesome/scss/',
                'node_modules/foundation-sites/scss/',
                './node_modules/slick-carousel/slick/'
            ],
        }).on('error', $.sass.logError))
        .pipe($.autoprefixer(AUTOPREFIXER_BROWSERS))
        .pipe(gulp.dest('.tmp/styles'))
        // Concatenate and minify styles
        .pipe($.if('*.css', $.cssnano()))
        .pipe($.size({title: 'styles'}))
        .pipe($.sourcemaps.write('./'))
        .pipe(gulp.dest('./public/assets/css'))
        .pipe(gulp.dest('.tmp/styles'));
});

gulp.task('mocks', () => {
    return gulp.src('src/mocks/**/*.json')
        .pipe(jsoncombine('mocks.json', function (data) {
            return new Buffer(JSON.stringify(data, null, 2));
        }))
        .pipe(gulp.dest('public'))
        .pipe(browserSync.stream());
});

gulp.task('pug', () => {
    global._ = require('lodash');
    global.mocks = require('./public/mocks.json');

    return gulp.src('src/pug/pages/*.pug')
        .pipe(pug({
            pretty: true,
            globals: ['_', 'mocks']
        }))
        .pipe(gulp.dest('public'));
});

let scripts = [
    'main.js'
];

scripts.forEach((script) => {
    gulp.task(script, () => {
        return browserifyScript(script);
    });
});

gulp.task('scripts', scripts);

gulp.task('images', () => {
    gulp.src('./src/img/favicon.ico')
        .pipe(gulp.dest('./public'));

    return gulp.src('./src/img/**/*')
        .pipe($.cache($.imagemin({
            progressive: true,
            interlaced: true
        })))
        .pipe(gulp.dest('./public/assets/img'))
        .pipe($.size({title: 'images'}));
});

gulp.task('vendor-scripts', () => {
    gulp.src(['./node_modules/jquery/dist/jquery.min.js',
              './node_modules/slick-carousel/slick/slick.min.js'])
        .pipe(gulp.dest('./public/assets/vendor'));
});

gulp.task('vendor', () => {
    gulp.src(['node_modules/fancybox/dist/**/*',
              '!node_modules/fancybox/dist/scss',
              '!node_modules/fancybox/dist/scss/**'])
        .pipe(gulp.dest('./public/assets/vendor/fancybox'));
});

gulp.task('fonts', () => {
    return gulp.src(['./src/fonts/**/*', './node_modules/font-awesome/fonts/**/*'])
        .pipe(gulp.dest('./public/assets/fonts'));
});

gulp.task('clean', () => del(['.tmp', './public/assets/*'], {dot: true}));

gulp.task('dev', ['build'], () => {
    browserSync({
        notify: false,
        server: 'public',
        port: 3000
    });

    gulp.watch(['./src/scss/**/*'], ['styles', reload]);
    gulp.watch(['./src/pug/**/*.pug'], ['pug', reload]);
    gulp.watch(['./src/mocks/**/*.json'], ['mocks', reload]);
    gulp.watch(['./src/js/**/*'], ['scripts', reload]);
    gulp.watch(['./src/img/**/*'], ['images', reload]);
    gulp.watch(['./public/**/*.html'], reload);
});

gulp.task('build', [], (cb) => {
    runSequence(
        'clean',
        ['styles', 'pug', 'mocks', 'fonts', 'images', 'scripts', 'vendor-scripts', 'vendor'],
        cb
    )
});
