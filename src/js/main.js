'use strict'

$(document).ready(function(){

    paintHeader();

    function paintHeader() {
        if ($(window).scrollTop() > 0) {
            $('.page-header-fixed').addClass('page-header--bg');
        } else {
            $('.page-header-fixed').removeClass('page-header--bg');
        }
    }

	$('.dropdown-toggle').click(function(e) {
		e.preventDefault()

		var $dropdownContent = $('#' + $(this).attr('data-toggle'));

		$dropdownContent.toggleClass('opened');

		$('.page-header').toggleClass('page-header--dropdown-opened')
	});

	$(window).on('scroll', function() {
		paintHeader();
	});


    $('.reviews-slider').slick({
        slidesToShow: 1,
        padding: 0,
        centerPadding: 0,
        centerMode: true,
        dots: true,
        arrows: true,
        initialSlide: 1,
        appendArrows: '.slick-arrows',
        prevArrow: '<button type="button" class="slick-prev"></button>',
        nextArrow: '<button type="button" class="slick-next"></button>',
        appendDots: '.reviews-slider-dots',
        customPaging: function(slider, i) {
    		var photo = $(slider.$slides[i]).data('photo');
    		return '<button type="button" style="background-image: url(assets/img/'+ photo +');"></button>'
        },
        responsive: [{
	        	breakpoint: 991,
	        	settings: {
	        		arrows: false
	        	}
        	}
        ]
    });

    $('.interface-slider').slick({
    	centerPadding: 0,
        centerMode: true,
        dots: false,
        arrows: false,
        slidesToShow: 3,
        variableWidth: true,
        autoplay: true,
        autoplaySpeed: 2500
    });

    $(".fancy-vid").fancybox({
        'fitToView'     : true,
        'transitionIn'  : 'none',
        'transitionOut' : 'none',
        'width'         : '100%',
        'height'        : '100%',
        'maxWidth'      : '780',
        'maxHeight'     : '600',
        'helpers' : {
            'media' : {}
        }
    });
});